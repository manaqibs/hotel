import React from 'react';
import BookingChart from './BookingChart';
import { connect } from 'react-redux';

const Home = (props) => {
  const arrayLength = props.recipeList.length;
  const hotelLength = props.hotelList.length;

  return (
    <div className="container-fluid dashboard-wrapper">
      <h1>Dashboard</h1>
      <div className="row total-numbers">
        <div className="col-md-3">
          <span className="recipeNumber">
            Recpies <i>{arrayLength}</i>{' '}
          </span>
        </div>
        <div className="col-md-3">
          <span className="reataurnetNumber">
            Restaurents <i>{hotelLength}</i>
          </span>
        </div>
        <div className="col-md-3">
          <span className="roomsNumber">
            Booked rooms <i>10</i>
          </span>
        </div>
        <div className="col-md-3">
          <span className="staffNumber">
            Sraff members <i>35</i>
          </span>
        </div>
      </div>
      <div className="row">
        <div className="col-md-3">Satisfaction rate</div>
        <div className="col-md-6">
          <div
            style={{
              boxShadow: '0px 1px 12px -3px #f3ecec',
              borderRadius: '10px',
              padding: '15px',
              fontWeight: '700',
            }}
          >
            Bookings
            <BookingChart />
          </div>
        </div>
        <div className="col-md-3">New guests</div>
      </div>
    </div>
  );
};

const mapStatetoProps = ({ recipe, hotels }) => {
  const { recipeList } = recipe;
  const { hotelList } = hotels;
  return { recipeList, hotelList };
};

export default connect(mapStatetoProps, null)(Home);
