import React from 'react';
import { connect } from 'react-redux';

const SingleRecipe = (props) => {
  const recipeId = props.location.data && props.location.data.id;
  const recipeObj = props.recipeList.find((recipe) => recipe.id === recipeId);

  const arrayLength = props.recipeList.length;
  console.log(arrayLength);

  return (
    <div className="container-fluid recipecard-detail">
      <h1>{recipeObj.name}</h1>
      <div className="row">
        <div className="col-md-6">
          <div
            className="recipecard-detail__pic"
            style={{ backgroundImage: `url(${recipeObj.image})` }}
          />
        </div>
        <div className="col-md-6">
          <h3>Ingrdients</h3>
          <ul className="recipecard-detail__ingredients">
            {recipeObj.ingredients.map((item, index) => (
              <li key={index}>{item}</li>
            ))}
          </ul>
          <ul className="recipecard-detail__serving-cost">
            <li>
              <span>Serving:</span>{' '}
              <span>
                {recipeObj.serving}{' '}
                {recipeObj.serving > 1 ? 'Persons' : 'Person'}
              </span>
            </li>
            <li>
              <span>Preparation time:</span>
              <span>{recipeObj.preparationtime}</span>
            </li>
            <li>
              <span>Cost:</span> <span>{recipeObj.cost}</span>
            </li>
          </ul>
        </div>
      </div>
      <div className="container-fluid">
        <div className="row pl-0">
          <div className="col-md-12">
            <h3>Recipe detail</h3>
            <div className="col-md-7 pl-0">
              <p>{recipeObj.desc}</p>
            </div>
          </div>
        </div>
        {/* <div className="row">
          <div className="col-md-12">
            <h3>Related recipes</h3>
            <div className="row">
              <div className="col-md-4">1</div>
              <div className="col-md-4">2</div>
              <div className="col-md-4">3</div>
            </div>
          </div>
        </div> */}
      </div>
    </div>
  );
};

const mapStatetoProps = ({ recipe }) => {
  const { recipeList } = recipe;
  return { recipeList };
};

export default connect(mapStatetoProps, null)(SingleRecipe);
