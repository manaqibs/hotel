import React from 'react';
import {
  Home,
  History,
  Staff,
  Restaurent,
  Rooms,
  Recipe,
  Login,
  SingleRecipe,
} from '../components/index';

import { Route, Switch } from 'react-router-dom';

const BodyContainer = () => {
  return (
    <div className="body-content">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/history" component={History} />
        <Route path="/Staff" component={Staff} />
        <Route path="/restaurent" component={Restaurent} />
        <Route path="/rooms" component={Rooms} />
        <Route path="/recipe" component={Recipe} />
        <Route path="/login" component={Login} />
        <Route path="/:url" component={SingleRecipe} />
      </Switch>
    </div>
  );
};

export default BodyContainer;
