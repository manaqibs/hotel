import React, { useState, createContext } from 'react';

export const HotelContext = createContext();
// export const RecipeContext = createContext();

export const HotelProvider = (props) => {
  const [recipes, setRecipies] = useState([
    {
      name: 'Showarma',
      ingredients: [
        'Hot sauce  1 tbsp',
        'Ketchup  1 tbsp',
        'Worcester sauce  3 tbsp',
        'Soy sauce  2 tbsp',
        'White vinegar  1 tbsp',
        'Salt  1/4 tsp or as required',
      ],
      image:
        'https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      serving: 7,
      desc:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
      cost: 1500,
      preparationtime: '30min',
    },
    {
      name: 'Chicken paties',
      ingredients: [
        'Garlic cloves  4',
        'Capsicum green and red  1',
        'Green onion   3',
        'Onion  1',
        'Green chili  3-4',
      ],
      image:
        'https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',

      serving: 4,
      desc:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
      cost: 1500,
      preparationtime: '25min',
    },
  ]);
  const [hotels, setHotel] = useState([
    {
      id: 1,
      name: 'Hotel Imperial',
      image:
        'https://images.pexels.com/photos/573552/pexels-photo-573552.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Karachi',
      star: 2,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'true',
      foodfacility: 'true',
      parkingfacility: 'true',
      recipe: {
        name: 'Showarma',
        ingredients: [
          'Hot sauce  1 tbsp',
          'Ketchup  1 tbsp',
          'Worcester sauce  3 tbsp',
          'Soy sauce  2 tbsp',
          'White vinegar  1 tbsp',
          'Salt  1/4 tsp or as required',
        ],
        image:
          'https://images.pexels.com/photos/461198/pexels-photo-461198.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 7,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '30min',
      },
    },
    {
      id: 2,
      name: 'Hotel Serena',
      image:
        'https://images.pexels.com/photos/164595/pexels-photo-164595.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Islamabad',
      star: 4,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'true',
      parkingfacility: 'true',
      recipe: {
        name: 'Chicken paties',
        ingredients: [
          'Garlic cloves  4',
          'Capsicum green and red  1',
          'Green onion   3',
          'Onion  1',
          'Green chili  3-4',
        ],
        image:
          'https://images.pexels.com/photos/376464/pexels-photo-376464.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',

        serving: 4,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '25min',
      },
    },
    {
      id: 3,
      name: 'PC',
      image:
        'https://images.pexels.com/photos/573552/pexels-photo-573552.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Karachi',
      star: 5,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'true',
      foodfacility: 'false',
      parkingfacility: 'true',
      recipe: {
        name: 'Dum biryani',
        ingredients: [
          'Beef (sirloin)  300 g',
          'Egg white of 1 egg',
          'Corn flour  1 tbsp',
          'Salt  1/4 tsp',
          'Black pepper  1/4 tsp',
        ],
        image:
          'https://images.pexels.com/photos/277253/pexels-photo-277253.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 6,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '55min',
      },
    },
    {
      id: 4,
      name: 'Marriott',
      image:
        'https://images.pexels.com/photos/271624/pexels-photo-271624.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Islamabad',
      star: 7,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'true',
      parkingfacility: 'true',
      recipe: {
        name: 'Sindhi biryani',
        ingredients: [
          'Hot sauce  1 tbsp',
          'Ketchup  1 tbsp',
          'Worcester sauce  3 tbsp',
          'Soy sauce  2 tbsp',
          'White vinegar  1 tbsp',
          'Salt  1/4 tsp or as required',
        ],
        image:
          'https://images.pexels.com/photos/1624487/pexels-photo-1624487.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 3,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '1hr',
      },
    },
    {
      id: 5,
      name: 'Regent Plaza',
      image:
        'https://images.pexels.com/photos/161758/governor-s-mansion-montgomery-alabama-grand-staircase-161758.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Karachi',
      star: 3,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'true',
      parkingfacility: 'true',
      recipe: {
        name: 'Burger',
        ingredients: [
          'Garlic cloves  4',
          'Capsicum green and red   1',
          'Green onion   3',
          'Onion  1',
          'Green chili  3-4',
        ],
        image:
          'https://images.pexels.com/photos/675951/pexels-photo-675951.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 8,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '20min',
      },
    },
    {
      id: 6,
      name: 'Sharaton',
      image:
        'https://images.pexels.com/photos/2034335/pexels-photo-2034335.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Karachi',
      star: 5,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'true',
      parkingfacility: 'true',
      recipe: {
        name: 'Club sandwich',
        ingredients: [
          'Beef (sirloin)  300 g',
          'Egg white of 1 egg',
          'Corn flour  1 tbsp',
          'Salt  1/4 tsp',
          'Black pepper  1/4 tsp',
        ],
        image:
          'https://images.pexels.com/photos/1600711/pexels-photo-1600711.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 4,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '2days',
      },
    },
    {
      id: 7,
      name: 'Deva',
      image:
        'https://images.pexels.com/photos/1579253/pexels-photo-1579253.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Karachi',
      star: 1,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'false',
      parkingfacility: 'false',
      recipe: {
        name: 'Daheballay',
        ingredients: [
          'Hot sauce  1 tbsp',
          'Ketchup  1 tbsp',
          'Worcester sauce  3 tbsp',
          'Soy sauce  2 tbsp',
          'White vinegar  1 tbsp',
          'Salt  1/4 tsp or as required',
        ],
        image:
          'https://images.pexels.com/photos/1095550/pexels-photo-1095550.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 2,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '30min',
      },
    },
    {
      id: 8,
      name: 'Blu Sky',
      image:
        'https://images.pexels.com/photos/1001965/pexels-photo-1001965.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Rawalpindi',
      star: 4,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'false',
      parkingfacility: 'false',
      recipe: {
        name: 'Chines',
        ingredients: [
          'Garlic cloves  4',
          'Capsicum green and red   1',
          'Green onion   3',
          'Onion  1',
          'Green chili  3-4',
        ],
        image:
          'https://images.pexels.com/photos/842571/pexels-photo-842571.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 7,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '15min',
      },
    },
    {
      id: 9,
      name: 'Usmania',
      image:
        'https://images.pexels.com/photos/1134176/pexels-photo-1134176.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Karachi',
      star: 3,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'false',
      parkingfacility: 'true',
      recipe: {
        name: 'Cake',
        ingredients: [
          'Hot sauce  1 tbsp',
          'Ketchup  1 tbsp',
          'Worcester sauce  3 tbsp',
          'Soy sauce  2 tbsp',
          'White vinegar  1 tbsp',
          'Salt  1/4 tsp or as required',
        ],
        image:
          'https://images.pexels.com/photos/45202/brownie-dessert-cake-sweet-45202.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 5,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '50min',
      },
    },
    {
      id: 10,
      name: 'King Fisher',
      image:
        'https://images.pexels.com/photos/827528/pexels-photo-827528.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
      location: 'Lahore',
      star: 7,
      capacity: 5000,
      roomtypes: ['single', 'double', 'double sweet'],
      featured: 'false',
      foodfacility: 'false',
      parkingfacility: 'false',
      recipe: {
        name: 'Malae boti',
        ingredients: [
          'Beef (sirloin)  300 g',
          'Egg white of 1 egg',
          'Corn flour  1 tbsp',
          'Salt  1/4 tsp',
          'Black pepper  1/4 tsp',
        ],
        image:
          'https://images.pexels.com/photos/718742/pexels-photo-718742.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
        serving: 3,
        desc:
          'Lorem ipsum dolor sit amet consectetur adipisicing elit. Itaque voluptatem architecto accusantium adipisci odit cumque inventore.Tempore aspernatur officiis eos culpa dolor eligendi saepe placeat delectus rem nisi corrupti quaerat.',
        cost: 1500,
        preparationtime: '65min',
      },
    },
  ]);
  return (
    <div>
      <HotelContext.Provider value={[hotels, setHotel]}>
        {props.children}
      </HotelContext.Provider>
    </div>
  );
};
