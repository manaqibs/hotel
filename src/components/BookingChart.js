import React, { Component } from 'react';
import ReactApexChart from 'react-apexcharts';

class BookingChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      series: [
        {
          name: 'Awaiting',
          data: [10, 11, 20, 25, 15, 21, 30, 28, 22],
        },
        {
          name: 'Occupied',
          data: [20, 21, 10, 15, 35, 41, 33, 22, 12],
        },
        {
          name: 'Available',
          data: [30, 31, 30, 5, 25, 10, 21, 37, 18],
        },
      ],
      options: {
        chart: {
          type: 'bar',
          height: 350,
          toolbar: {
            tools: {
              download: false,
            },
          },
        },
        plotOptions: {
          bar: {
            horizontal: false,
            columnWidth: '55%',
            endingShape: 'rounded',
          },
        },
        dataLabels: {
          enabled: false,
        },
        stroke: {
          show: true,
          width: 2,
          colors: ['transparent'],
        },
        xaxis: {
          categories: [
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
          ],
        },
        yaxis: {
          title: {
            text: 'Days',
          },
        },
        fill: {
          opacity: 1,
        },

        // tooltip: {
        //   y: {
        //     formatter: function (val) {
        //       return '$ ' + val + ' thousands';
        //     },
        //   },
        // },
      },
    };
  }

  render() {
    return (
      <div>
        <div id="chart">
          <ReactApexChart
            options={this.state.options}
            series={this.state.series}
            type="bar"
            height={350}
          />
        </div>
        <div id="html-dist"></div>
      </div>
    );
  }
}

export default BookingChart;
