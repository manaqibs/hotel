export { default as Home } from './Home';
export { default as History } from './History';
export { default as Staff } from './Staff';
export { default as Restaurent } from './Restaurent';
export { default as Rooms } from './Rooms';
export { default as Recipe } from './Recipe';
export { default as Login } from './Login';
export { default as SingleRecipe } from './SingleRecipe';
