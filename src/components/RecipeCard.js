import React from 'react';
import { Link } from 'react-router-dom';
import { FaPlusCircle, FaRegEdit } from 'react-icons/fa';

function RecipeCard({ name, image, serving, id }) {
  return (
    <div className="recipecard">
      <span
        className="recipecard__pic"
        style={{ backgroundImage: `url(${image})` }}
      ></span>
      <span className="recipecard__name">{name}</span>

      <span className="recipecard__serving">
        <i>
          Serving: <span>{serving}</span>
        </i>
        {/* <i>
          ID: <span>{id}</span>
        </i> */}
      </span>
      <div className="overlay">
        <span className="edit-record">Edit recipe</span>
        <Link to={{ pathname: '/single-recipe', data: { id } }}>
          Show detail
        </Link>
      </div>
    </div>
  );
}

export default RecipeCard;
