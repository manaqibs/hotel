import React from 'react';
import {
  AiOutlineHome,
  AiOutlineHistory,
  AiOutlineBank,
  AiOutlineLaptop,
  AiOutlinePoweroff,
} from 'react-icons/ai';
import { MdRestaurantMenu } from 'react-icons/md';
import {
  IoIosRestaurant,
  IoMdNotificationsOutline,
  IoIosPeople,
} from 'react-icons/io';

import { Link } from 'react-router-dom';

const LeftNav = (props) => {
  return (
    <div className="left-nav-bar align-items-center">
      <Link to="/">
        <span>
          <AiOutlineHome />
          <label className="name-label">Home</label>
        </span>
      </Link>
      {/* <Link to="/history">
        <span>
          <AiOutlineHistory />
          <label className="name-label">History</label>
        </span>
      </Link> */}
      <Link to="/staff">
        <span>
          <IoIosPeople />
          <label className="name-label">Staff</label>
        </span>
      </Link>
      <Link to="/rooms">
        <span>
          <AiOutlineBank />
          <label className="name-label">Rooms</label>
        </span>
      </Link>
      <Link to="/restaurent">
        <span>
          <IoIosRestaurant />
          <label className="name-label">Restaurent</label>
        </span>
      </Link>
      <Link to="/recipe">
        <span>
          <MdRestaurantMenu />
          <label className="name-label">Recipe</label>
        </span>
      </Link>
      <div className="notifications">
        <span>
          <IoMdNotificationsOutline />
          <label className="name-label">Notifications</label>
          <span className="badge badge-light badge-number">0</span>
        </span>
        <Link to="./login">
          <span>
            <AiOutlinePoweroff />
            <label className="name-label">Login</label>
          </span>
        </Link>
      </div>
    </div>
  );
};

export default LeftNav;
