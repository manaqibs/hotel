import React from 'react';
import HotelCard from './HotelCard';
import { connect } from 'react-redux';

const Restaurent = (props) => {
  return (
    <div className="container-fluid">
      <h1>Restaurents</h1>
      <div className="row">
        {props.hotelList.map((hotel) => (
          <div className="col-md-3" key={hotel.id}>
            <HotelCard
              name={hotel.name}
              image={hotel.image}
              star={hotel.star}
              location={hotel.location}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = ({ hotels }) => {
  const { hotelList } = hotels;
  return { hotelList };
};

export default connect(mapStateToProps, null)(Restaurent);
