import hotelsReducer from './hotelsReducde';
import recipeReducer from './recipeReducde';

import { combineReducers } from 'redux';

const rootRedcer = combineReducers({
  hotels: hotelsReducer,
  recipe: recipeReducer,
});

export default rootRedcer;
