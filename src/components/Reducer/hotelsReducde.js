import hotelList from '../hotel.json';

const INITIAL_STATE = {
  hotelList: hotelList
};

const HotelReducer = (state, action) => {
  if (!state) state = INITIAL_STATE;

  switch (action.type) {
    case 'HOTEL_LIST':
      return !state;

    default:
      return state;
  }
};

export default HotelReducer;
