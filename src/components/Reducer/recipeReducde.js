import recipeList from '../recipe.json';

const INITIAL_STATE = {
  recipeList: recipeList,
};

const RecipeReducer = (state, action) => {
  if (!state) state = INITIAL_STATE;

  switch (action.type) {
    case 'RECIPE_LIST':
      return !state;

    default:
      return state;
  }
};

export default RecipeReducer;
