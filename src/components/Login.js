import React from 'react';
import { AiOutlineLeft } from 'react-icons/ai';
import { Link } from 'react-router-dom';
import AppInput from './common/AppInput';
import { MdRestaurantMenu } from 'react-icons/md';

const Login = () => {
  return (
    <div className="login-wrapper">
      <Link to="/" className="back-btn">
        <AiOutlineLeft /> Back
      </Link>
      <div className="login-inner">
        <h2 className="text-center ">
          <MdRestaurantMenu />
        </h2>
        <form>
          <div class="form-group">
            <AppInput
              type="email"
              className="form-control"
              id="exampleInputEmail1"
              placeholder="Enter email"
            />
          </div>
          <div class="form-group">
            <AppInput
              type="password"
              className="form-control"
              id="exampleInputPassword1"
              placeholder="Password"
            />
          </div>

          <button type="submit" class="btn btn-secondary">
            Submit
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
