import React from 'react';
import { MdAddCircle } from 'react-icons/md';

const AppAddButton = ({ children, onClick }) => {
  return (
    <>
      <button onClick={onClick}>
        <MdAddCircle className="add-btn" />
        {children}
      </button>
    </>
  );
};

export default AppAddButton;
