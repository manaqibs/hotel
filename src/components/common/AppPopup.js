import React from 'react';
const AppPopup = ({ onClick, children }) => {
  return (
    <div className="popup-overlay">
      <div className="popup-content">
        <span>{children}</span>
      </div>
    </div>
  );
};

export default AppPopup;
