import React, { useState } from 'react';
import RecipeCard from './RecipeCard';
import { connect } from 'react-redux';
import { FaPlus } from 'react-icons/fa';
import { AiOutlineCloseCircle } from 'react-icons/ai';
import AppPopup from '../components/common/AppPopup';

const Recipe = (props) => {
  const [addRecipe, setAddRecipe] = useState(false);

  return (
    <div className="container-fluid">
      {addRecipe && (
        <AppPopup>
          <span className="popup-close">
            <AiOutlineCloseCircle onClick={() => setAddRecipe(false)} />
          </span>
          <form action="/action_page.php">
            <div className="form-group">
              <label for="imageurl">Image URL:</label>
              <input type="text" className="form-control" id="imageurl" />
            </div>
            <div className="form-group">
              <label for="recipe">Recipe name:</label>
              <input type="text" className="form-control" id="recipe" />
            </div>
            <div className="form-group">
              <label for="serving">Serving:</label>
              <input type="text" className="form-control" id="serving" />
            </div>
          </form>
          <div className="button-wrapepr">
            <button
              className="btn btn-primary"
              onClick={() => setAddRecipe(false)}
            >
              Submit
            </button>
          </div>
        </AppPopup>
      )}
      <h1>
        Recipe{' '}
        <span className="add-recipe">
          <FaPlus onClick={() => setAddRecipe(true)} />
        </span>
      </h1>
      <div className="row">
        {props.recipeList.map((recipe) => (
          <div className="col-md-3" key={recipe.id}>
            <RecipeCard
              name={recipe.name}
              image={recipe.image}
              serving={recipe.serving}
              id={recipe.id}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

const mapStateToProps = ({ recipe }) => {
  const { recipeList } = recipe;
  return { recipeList };
};

export default connect(mapStateToProps, null)(Recipe);
