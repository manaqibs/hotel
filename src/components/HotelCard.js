import React from 'react';

function HotelCard({ name, location, image, star }) {
  return (
    <div className="hotelcard">
      <span
        className="hotelcard__pic"
        style={{ backgroundImage: `url(${image})` }}
      ></span>
      <span className="hotelcard__name">{name}</span>
      <div className="hotelcard__locstar">
        <span className="hotelcard__locstar-location">{location}</span>
        <span className="hotelcard__locstar-star">{star} Star</span>
      </div>
    </div>
  );
}

export default HotelCard;
