import React from 'react';
import BodyContainer from './components/BodyContainer';
import LeftNav from './components/LeftNav';
import AppHeader from './components/common/AppHeader';
import { HotelProvider } from './components/HotelContext';

import './resources/css/style.css';

function App() {
  return (
    <HotelProvider>
      <div className="App">
        <LeftNav />
        {/* <AppHeader /> */}
        <BodyContainer />
      </div>
    </HotelProvider>
  );
}

export default App;
